// -----------------------------------------------------------------------------
//
// BUGS:
//
// TODO:
//  - add generic hotkey "i" for info about currently selected shot,
//      selected asset, selected camera (special/shot), ...?
//
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
class CModSbUiPopupCallback extends IModUiConfirmPopupCallback {
    public var callback: CStoryBoardUiMod;

    public function OnConfirmed(action: String) {
        switch (action) {
            case "quit": return callback.doQuit();
        }
    }
}
// ----------------------------------------------------------------------------
state SbUi_Active in CStoryBoardUiMod {
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        parent.modUtil.deactivateHud();
        parent.modUtil.hidePlayer();
        parent.modUtil.freezeTime();

        // refresh camera position to player position
        parent.radSbui.activate(parent.sceneId);
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        theInput.SetContext('Exploration');
        parent.radSbui.deactivate();

        parent.modUtil.unfreezeTime();
        parent.modUtil.restorePlayer();
        parent.modUtil.reactivateHud();
    }
    // ------------------------------------------------------------------------
    event OnMinimize(action: SInputAction) {
        if (IsPressed(action)) {
            if (action.aName == 'SBUI_Minimize') {
                parent.GotoState('SbUi_Minimized', false, true);
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state SbUi_Minimized in CStoryBoardUiMod {
    // ------------------------------------------------------------------------
    event OnMaximize(action: SInputAction) {
        var entity : CEntity;
        var template : CEntityTemplate;

        if (IsPressed(action)) {
            if (!parent.radSbui) {
                template = (CEntityTemplate)LoadResource("dlc\modtemplates\storyboardui\radsbui.w3mod", true);
                entity = theGame.CreateEntity(template,
                    thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());

                parent.radSbui = (CRadishStoryBoardUi)entity;
                parent.radSbui.init(
                    parent,
                    parent.log,
                    parent.quitConfirmCallback,
                    parent.sceneId);

                template = (CEntityTemplate)LoadResource("dlc\modtemplates\radishseeds\radish_modutils.w2ent", true);
                entity = theGame.CreateEntity(template,
                    thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());
                parent.modUtil = (CRadishModUtils)entity;
            }

            parent.PushState('SbUi_Active');
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
statemachine class CStoryBoardUiMod extends CMod {
    default modName = 'StoryBoardUi';
    default modAuthor = "rmemr, erxv";
    default modUrl = "http://www.nexusmods.com/witcher3/mods/2114/";
    default modVersion = '0.8.0';

    default logLevel = MLOG_DEBUG;
    // ------------------------------------------------------------------------
    protected var radSbui: CRadishStoryBoardUi;
    // optional filter for saved storyboards
    protected var sceneId: CName;

    protected var modUtil: CRadishModUtils;
    // ------------------------------------------------------------------------
    // UI stuff
    protected var quitConfirmCallback: CModSbUiPopupCallback;
    // ------------------------------------------------------------------------
    private var gameTime: GameTime;
    // ------------------------------------------------------------------------
    public function init() {
        super.init();

        this.registerListeners();

        // prepare view callback wiring
        quitConfirmCallback = new CModSbUiPopupCallback in this;
        quitConfirmCallback.callback = this;

        // store time on activation to reset from any interactive time changes
        gameTime = theGame.GetGameTime();
        PushState('SbUi_Minimized');
    }
    // ------------------------------------------------------------------------
    public function initWithSceneId(optional sceneId: CName) {
        switch (this.GetCurrentStateName()) {
            case 'SbUi_Active':
                theGame.GetGuiManager().ShowNotification(GetLocStringByKeyExt("SBUI_eMinimizeSbuiFirst"));
                break;

            case 'SbUi_Minimized':
                this.sceneId = sceneId;
                OnMaximize(SInputAction('simulated', 1.0, 0.0));
                break;

            case 'None':
                this.sceneId = sceneId;
                this.init();
                OnMaximize(SInputAction('simulated', 1.0, 0.0));
                break;
        }
    }
    // ------------------------------------------------------------------------
    public function repositionOrigin(
        originPos: Vector, originRot: EulerAngles, optional scenepointTag: String)
    {
        if (this.radSbui) {
            this.radSbui.repositionOrigin(originPos, originRot, scenepointTag);
        }
    }
    // ------------------------------------------------------------------------
    event OnMinimize(action: SInputAction) {}
    event OnMaximize(action: SInputAction) {}
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        theInput.RegisterListener(this, 'OnMinimize', 'SBUI_Minimize');
        theInput.RegisterListener(this, 'OnMaximize', 'SBUI_Maximize');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        theInput.UnregisterListener(this, 'SBUI_Minimize');
        theInput.UnregisterListener(this, 'SBUI_Maximize');
    }
    // ------------------------------------------------------------------------
    public function doQuit() {
        var defaultSceneId: CName;
        var null: CRadishStoryBoardUi;

        // reset every change made to time
        theGame.SetGameTime(gameTime, true);

        this.radSbui.doQuit();

        PopState();

        this.sceneId = defaultSceneId;
        this.modUtil.Destroy();
        this.radSbui.Destroy();
        this.radSbui = null;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CRadishStoryBoardUi extends CEntity {
    // ------------------------------------------------------------------------
    private var autoLogInterval: float; default autoLogInterval = 300.0;
    private var deferredAutoSaveRequired: bool;
    private var isSbUiActive: bool;
    // ------------------------------------------------------------------------
    // reference to parent in case it's searched by tag
    private var w3mod: CStoryBoardUiMod;
    // ------------------------------------------------------------------------
    private var log: CModLogger;
    // ------------------------------------------------------------------------
    private var sceneId: CName;
    // ------------------------------------------------------------------------
    // UI stuff
    private var confirmPopup: CModUiActionConfirmation;
    private var quitConfirmCallback: CModSbUiPopupCallback;
    // ------------------------------------------------------------------------
    // the real stuff
    private var storyboard: CModStoryBoard;
    private var currentMode: CModStoryBoardWorkMode;
    private var modeCallback: CModSbUiParentCallback;
    // ------------------------------------------------------------------------
    public function init(
        sbuimod: CStoryBoardUiMod,
        log: CModLogger,
        quitConfirmCallback: CModSbUiPopupCallback,
        sceneId: CName)
    {
        //var stateData: CModStoryBoardStateData;

        GetWitcherPlayer().DisplayHudMessage(GetLocStringByKeyExt("SBUI_Started"));

        this.w3mod = sbuimod;
        this.log = log;
        this.quitConfirmCallback = quitConfirmCallback;
        this.sceneId = sceneId;

        this.registerListeners();

        modeCallback = new CModSbUiParentCallback in this;
        modeCallback.callback = this;

        AddTimer('autoLogDefinition', this.autoLogInterval, true, , , , true);
    }
    // ------------------------------------------------------------------------
    function initOverviewMode() {
        var stateData: CModStoryBoardStateData;

        this.log.info("initOverviewMode " + sceneId);
        stateData = (CModStoryBoardStateData)GetModStorage().load('StoryBoardUi', sceneId);

        storyboard = new CModStoryBoard in this;
        storyboard.init(this.log, stateData);

        currentMode = new CModStoryBoardOverviewMode in this;
        currentMode.setParent(modeCallback);
        currentMode.init(storyboard);
    }
    // ------------------------------------------------------------------------
    public function getSbuiMod() : CStoryBoardUiMod {
        return this.w3mod;
    }
    // ------------------------------------------------------------------------
    public function activate(sceneId: CName) {
        this.sceneId = sceneId;

        this.initOverviewMode();

        storyboard.activate();

        ((CModStoryBoardOverviewMode)currentMode).activateDeferred(storyboard.getCurrentShot());

        this.isSbUiActive = true;
    }
    // ------------------------------------------------------------------------
    public function repositionOrigin(
        originPos: Vector, originRot: EulerAngles, optional scenepointTag: String)
    {
        storyboard.repositionOrigin(originPos, originRot, scenepointTag);
        GetWitcherPlayer().DisplayHudMessage(
            GetLocStringByKeyExt("SBUI_OriginOverwrite")
            + VecToString(originPos));
    }
    // ------------------------------------------------------------------------
    public function deactivate() {
        if (isSbUiActive) {
            this.isSbUiActive = false;

            currentMode.showUi(false);
            saveStoryboardState();
            storyboard.reset();
        }
    }
    // ------------------------------------------------------------------------
    timer function autoLogDefinition(deltaTime: float, id: int) {
        if (isSbUiActive) {
            // only autolog definitions in overview mode since afterwards the shots
            // needs to be reset (legacy reasons)
            if (currentMode.getId() == 'SBUI_ModeOverview') {
                doLogDefinition();
            } else {
                deferredAutoSaveRequired = true;
            }
        }
    }
    // ------------------------------------------------------------------------
    private function doLogDefinition() {
        this.log.info("auto-saving definitions to scriptlog...");

        storyboard.saveW2SceneDescripton();
        theGame.GetGuiManager().ShowNotification(GetLocStringByKeyExt("SBUI_iAutoDefinitionLogged"));

        deferredAutoSaveRequired = false;
    }
    // ------------------------------------------------------------------------
    function saveStoryboardState() {
        var sbuiState: CModStoryBoardStateData;

        sbuiState = storyboard.getState();
        sbuiState.containerId = sceneId;

        GetModStorage().save(sbuiState);
    }
    // ------------------------------------------------------------------------
    public function doQuit() {
        var null: CStoryBoardUiMod;

        RemoveTimer('autoLogDefinition');

        storyboard.saveW2SceneDescripton();

        unregisterListeners();

        deactivate();
        this.w3mod = null;
        GetWitcherPlayer().DisplayHudMessage(GetLocStringByKeyExt("SBUI_Stopped"));
    }
    // ------------------------------------------------------------------------
    public function quitRequest() {
        var msgTitle: String;
        var msgText: String;

        if (confirmPopup) { delete confirmPopup; }

        confirmPopup = new CModUiActionConfirmation in this;

        if (storyboard.hasUnsavedChanges()) {
            msgTitle = "SBUI_tQuitConfirmUnsaved";
            msgText = "SBUI_mQuitConfirmUnsaved";
        } else {
            msgTitle = "SBUI_tQuitConfirm";
            msgText = "SBUI_mQuitConfirm";
        }
        confirmPopup.open(quitConfirmCallback,
            GetLocStringByKeyExt(msgTitle),
            GetLocStringByKeyExt(msgText), "quit");
    }
    // ------------------------------------------------------------------------
    event OnQuitRequest(action: SInputAction) {
        if (IsPressed(action)) {
            quitRequest();
        }
    }
    // ------------------------------------------------------------------------
    event OnHelpMePrettyPlease(action: SInputAction) {
        var helpPopup: CModUiHotkeyHelp;
        var titleKey: String;
        var introText: String;
        var hotkeyList: array<SModUiHotkeyHelp>;

        if (IsPressed(action)) {
            helpPopup = new CModUiHotkeyHelp in this;

            titleKey = "SBUI_tHelpHotkey";
            introText = "<p align=\"left\">" + GetLocStringByKeyExt("SBUI_mHelpCurrentWorkmode") + currentMode.getName() + "</p>";
            introText += "<p>" + currentMode.getGeneralHelp() + "</p>";

            // this must be available all the time
            hotkeyList.PushBack(HotkeyHelp_from('SBUI_ShowHelp'));
            // QUIT is only available in overview mode!
            if (currentMode.getId() == 'SBUI_ModeOverview') {
                hotkeyList.PushBack(HotkeyHelp_from('SBUI_Quit'));
            }
            // TODO remove currentMode from Hotkeylist?
            hotkeyList.PushBack(HotkeyHelp_from('SBUI_ModeOverview'));
            hotkeyList.PushBack(HotkeyHelp_from('SBUI_ModeAssets'));
            hotkeyList.PushBack(HotkeyHelp_from('SBUI_ModeCamera'));
            hotkeyList.PushBack(HotkeyHelp_from('SBUI_ModePlacement'));
            hotkeyList.PushBack(HotkeyHelp_from('SBUI_ModeAnimation'));
            hotkeyList.PushBack(HotkeyHelp_from('SBUI_ModeMimics'));
            hotkeyList.PushBack(HotkeyHelp_from('SBUI_ModeLookAt'));
            hotkeyList.PushBack(HotkeyHelp_from('SBUI_ModeEffects'));
            hotkeyList.PushBack(HotkeyHelp_from('SBUI_ModeVoiceLines'));

            currentMode.OnHotkeyHelp(hotkeyList);

            helpPopup.open(titleKey, introText, hotkeyList);
        }
    }
    // ------------------------------------------------------------------------
    public function doChangeWorkMode(workMode: CName, saveSettings: bool) {
        if (saveSettings) {
            currentMode.storeSettings();
            storyboard.refreshViewer();
        } else if (currentMode.getId() == 'SBUI_ModeCamera') {
            // camera mode uses its own camera and deactivates the shot cam
            // -> reactivate it (even if no settings changed)
            storyboard.refreshViewer();
        }

        currentMode.deactivate();
        delete currentMode;

        switch (workMode) {
            case 'SBUI_ModeOverview':
                currentMode = new CModStoryBoardOverviewMode in this;
                currentMode.init(storyboard);
                break;

            case 'SBUI_ModeAssets':
                currentMode = new CModStoryBoardAssetWorkMode in this;
                currentMode.init(storyboard);
                break;

            case 'SBUI_ModeCamera':
                currentMode = new CModStoryBoardCameraMode in this;
                currentMode.init(storyboard);
                break;

            case 'SBUI_ModePlacement':
                currentMode = new CModStoryBoardPlacementMode in this;
                currentMode.init(storyboard);
                break;

            case 'SBUI_ModeAnimation':
                currentMode = new CModStoryBoardAnimationMode in this;
                currentMode.init(storyboard);
                break;

            case 'SBUI_ModeMimics':
                currentMode = new CModStoryBoardMimicsMode in this;
                currentMode.init(storyboard);
                break;

            case 'SBUI_ModeLookAt':
                currentMode = new CModStoryBoardLookAtMode in this;
                currentMode.init(storyboard);
                break;

			case 'SBUI_ModeEffects':
                currentMode = new CModStoryBoardEffectsMode in this;
                currentMode.init(storyboard);
                break;

            case 'SBUI_ModeVoiceLines':
                currentMode = new CModStoryBoardVoiceLinesMode in this;
                currentMode.init(storyboard);
                break;
        }
        currentMode.setParent(modeCallback);
        currentMode.activate(storyboard.getCurrentShot());
        // only autolog definitions in overview mode since afterwards the shots
        // needs to be reset (legacy reasons)
        if (deferredAutoSaveRequired && workMode == 'SBUI_ModeOverview') {
            doLogDefinition();
        }
    }
    // ------------------------------------------------------------------------
    event OnChangeWorkMode(action: SInputAction) {
        if (IsPressed(action) && currentMode.getId() != action.aName) {
            // top level work mode changes. current work mode must cleanup if it
            // has subviews and confirm successfull "leaving"

            //TODO cleanup request first?
            if (currentMode.hasModifiedSettings()) {
                // FIXME!!!
                doChangeWorkMode(action.aName, true);
            } else {
                doChangeWorkMode(action.aName, false);
            }
        }
    }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        // -- generic hotkeys
        theInput.RegisterListener(this, 'OnHelpMePrettyPlease', 'SBUI_ShowHelp');
        theInput.RegisterListener(this, 'OnQuitRequest', 'SBUI_Quit');

        // -- supported workmodes
        theInput.RegisterListener(this, 'OnChangeWorkMode', 'SBUI_ModeOverview');
        theInput.RegisterListener(this, 'OnChangeWorkMode', 'SBUI_ModeAssets');
        theInput.RegisterListener(this, 'OnChangeWorkMode', 'SBUI_ModePlacement');
        theInput.RegisterListener(this, 'OnChangeWorkMode', 'SBUI_ModeAnimation');
        theInput.RegisterListener(this, 'OnChangeWorkMode', 'SBUI_ModeCamera');
        theInput.RegisterListener(this, 'OnChangeWorkMode', 'SBUI_ModeMimics');
        theInput.RegisterListener(this, 'OnChangeWorkMode', 'SBUI_ModeLookAt');
        theInput.RegisterListener(this, 'OnChangeWorkMode', 'SBUI_ModeEffects');
        theInput.RegisterListener(this, 'OnChangeWorkMode', 'SBUI_ModeVoiceLines');
        //...
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        // -- generic hotkeys
        theInput.UnregisterListener(this, 'SBUI_Quit');
        theInput.UnregisterListener(this, 'SBUI_ShowHelp');

        // -- supported workmodes
        theInput.UnregisterListener(this, 'SBUI_ModeOverview');
        theInput.UnregisterListener(this, 'SBUI_ModeAssets');
        theInput.UnregisterListener(this, 'SBUI_ModePlacement');
        theInput.UnregisterListener(this, 'SBUI_ModeAnimation');
        theInput.UnregisterListener(this, 'SBUI_ModeCamera');
        theInput.UnregisterListener(this, 'SBUI_ModeMimics');
        theInput.UnregisterListener(this, 'SBUI_ModeLookAt');
        theInput.UnregisterListener(this, 'SBUI_ModeEffects');
        theInput.UnregisterListener(this, 'SBUI_ModeVoiceLines');
        //...
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
function modCreate_StoryBoardUi() : CStoryBoardUiMod {
    var sbui: CRadishStoryBoardUi;

    sbui = (CRadishStoryBoardUi)theGame.GetEntityByTag('RadStoryboardUi');

    if (sbui) {
        return sbui.getSbuiMod();
    } else {
        return new CStoryBoardUiMod in thePlayer;
    }
}
// ----------------------------------------------------------------------------
exec function sbui(optional sceneId: CName) {
    var mod: CStoryBoardUiMod;
    mod = modCreate_StoryBoardUi();
    mod.initWithSceneId(sceneId);
}
// ----------------------------------------------------------------------------
exec function sbui_with_origin(
    x: Float, y: Float, z: Float, pitch: float, yaw: float, roll: float,
    optional sceneId: CName)
{
    var mod: CStoryBoardUiMod;

    mod = modCreate_StoryBoardUi();
    mod.initWithSceneId(sceneId);
    mod.repositionOrigin(Vector(x, y, z), EulerAngles(pitch, yaw, roll));
}
// ----------------------------------------------------------------------------
exec function sbui_with_scenepoint(tag: CName, optional sceneId: CName) {
    var scenepoint: CEntity;
    var mod: CStoryBoardUiMod;
    var pos: Vector;
    var rot: EulerAngles;

    scenepoint = (CEntity)theGame.GetEntityByTag(tag);

    if (scenepoint) {
        mod = modCreate_StoryBoardUi();

        pos = scenepoint.GetWorldPosition();
        rot = scenepoint.GetWorldRotation();

        mod.initWithSceneId(sceneId);
        mod.repositionOrigin(pos, rot, "SCENEPOINT:" + tag);
    } else {
        theGame.GetGuiManager().ShowNotification(GetLocStringByKeyExt("SBUI_ScenepointNotFound"));
    }
}
// ----------------------------------------------------------------------------
exec function sbui_list() {
    var viewCallback: CModSbUiPopupCallback;
    var infoPopup: CModUiActionConfirmation;
    var msg, id: String;
    var boardIds: array<CName>;
    var s, i: int;

    infoPopup = new CModUiActionConfirmation in thePlayer;

    boardIds = GetModStorage().listContainerIds('StoryBoardUi');
    s = boardIds.Size();

    if (s == 0) {
        msg = GetLocStringByKeyExt("SBUI_iNoStoredBoards");
    } else {
        msg = "<p align=\"left\">" + GetLocStringByKeyExt("SBUI_iFoundStoredBoards")
            + "</p></br></br><p align=\"left\"><ul>";

        for (i = 0; i < s; i += 1) {
            if (boardIds[i] != '') {
                id = boardIds[i];
            } else {
                id = "[default]";
            }
            msg += "<li>" + id + "</li>";
        }

        msg += "</ul></p>";
    }

    infoPopup.open(viewCallback, GetLocStringByKeyExt("SBUI_tStoredBoardsList"), msg, "ok");
}
// ----------------------------------------------------------------------------
function sbui_clear_storyboard(optional sceneId: CName) {
    var caption: String;

    if (sceneId){
        caption = sceneId;
    } else {
        caption = "[default]";
    }
    if (GetModStorage().remove('StoryBoardUi', sceneId)) {
        theGame.GetGuiManager().ShowNotification(GetLocStringByKeyExt("SBUI_BoardDeleted") + " " + caption);
    } else {
        theGame.GetGuiManager().ShowNotification(GetLocStringByKeyExt("SBUI_BoardNotDeleted") + " " + caption);
    }
}
// ----------------------------------------------------------------------------
exec function sbui_clear(optional sceneId: CName) {
    sbui_clear_storyboard(sceneId);
}
// ----------------------------------------------------------------------------
exec function sbui_clear_board(optional sceneId: CName) {
    sbui_clear_storyboard(sceneId);
}
// ----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
//
// BUGS:
//
// TODO:
//
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
class CModSbUiIdlePoseList extends CModUiFilteredList {
    // ------------------------------------------------------------------------
    public function createCompatibleList(
        actor: CModStoryBoardActor, idlePoseInfo: CStoryBoardIdlePoseMetaInfo) : int
    {
        var i: int;

        items.Clear();

        // first entry of animation lists is defined as no/default pose (id == 0)!
        items.PushBack(SModUiCategorizedListItem(
            0,
            idlePoseInfo.idlePoseList[0].caption,
            idlePoseInfo.idlePoseList[0].cat1,
            idlePoseInfo.idlePoseList[0].cat2,
            idlePoseInfo.idlePoseList[0].cat3,
        ));

        // create a compatible list of animations by actor
        for (i = 1; i < idlePoseInfo.idlePoseList.Size(); i += 1) {

            if (actor.isCompatibleAnimation(idlePoseInfo.idlePoseList[i].id)) {
                items.PushBack(SModUiCategorizedListItem(
                    idlePoseInfo.idlePoseList[i].slotId,
                    idlePoseInfo.idlePoseList[i].caption,
                    idlePoseInfo.idlePoseList[i].cat1,
                    idlePoseInfo.idlePoseList[i].cat2,
                    idlePoseInfo.idlePoseList[i].cat3,
                ));
            }
        }

        // anim compatibility probing plays animations -> last animation will
        // play to the end. looks strange -> prevent this
        actor.resetCompatibilityCheckAnimations();

        return items.Size();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
struct SStoryBoardIdlePoseInfo {
    var type: int;
    var cat1: String;
    var cat2: String;
    var cat3: String;
    var id: CName;
    var caption: String;
    // requried for w2scenes
    var posename: String;
    var emoState: String;
    var status: String;
    var slotId: int;
}
// ----------------------------------------------------------------------------
struct SStoryBoardDefaultPoseInfo {
    var uId: int;
    var type: int;
    var animId: CName;
}
// ----------------------------------------------------------------------------
// Wrapper class so list can be passed by reference
class CStoryBoardIdlePoseMetaInfo {
    // contains info about all idle pose animations. the slot number for a pose
    // anim be used as id in the filtered UI listview. this is required as the UI
    // returns the selected option id as string and there is no string -> name
    // conversion available but playing animations requires the anim name as CName.
    // meaning: this array is also used as ui selected anim id -> cname anim id LUT
    public var idlePoseList: array<SStoryBoardIdlePoseInfo>;
    // list of preselected default idle anims to probe right after actor creation
    // to use as default "pose" and prevent strange behavior (actor moving/rotating
    // triggered by behTrees after teleporting)
    public var defaultPoseList: array<SStoryBoardDefaultPoseInfo>;
    // ------------------------------------------------------------------------
    public function loadCsv(path: String) {
        var data: C2dArray;
        var i, actorType: int;

        data = LoadCSV(path);

        defaultPoseList.Clear();

        // csv: default;type;CAT1;CAT2;CAT3;id;caption;posename;emoState;status;
        for (i = 0; i < data.GetNumRows(); i += 1) {
            actorType = StringToInt(data.GetValueAt(1, i), -1);
            if (data.GetValueAt(0, i) != "") {
                defaultPoseList.PushBack(SStoryBoardDefaultPoseInfo(
                    i + 1, actorType, data.GetValueAtAsName(5, i)));
            }

            idlePoseList.PushBack(SStoryBoardIdlePoseInfo(
                actorType,
                data.GetValueAt(2, i),
                data.GetValueAt(3, i),
                data.GetValueAt(4, i),
                data.GetValueAtAsName(5, i),
                data.GetValueAt(6, i),
                data.GetValueAt(7, i),
                data.GetValueAt(8, i),
                data.GetValueAt(9, i),
                // since extra animations are added as top category the slot
                // position in the animList array does not match the numerical
                // part of the repo animid anymore
                // therefore explicitely store the slot from the vanilla data csv
                // to be used for id generation
                i + 1,
            ));
        }
    }
    // ------------------------------------------------------------------------
    public function addExtraAnimations(extraAnims: array<SSbUiExtraAnimation>) : int {
        var topCat: String;
        var i: int;

        idlePoseList.Clear();
        // provide entry for "empty" (aka no) animation
        idlePoseList.PushBack(SStoryBoardIdlePoseInfo(,,,,'default', "-default pose-",,,));

        topCat = GetLocStringByKeyExt("SBUI_ExtraPoseCat");

        for (i = 0; i < extraAnims.Size(); i += 1) {
            idlePoseList.PushBack(SStoryBoardIdlePoseInfo(
                -1,
                topCat,
                extraAnims[i].subCategory1,
                extraAnims[i].subCategory2,
                extraAnims[i].animName,
                extraAnims[i].caption,
                ,
                ,
                ,
                // custom animids always start from 100000 so they do not collide
                // with vanilla repo ids (which use up to ~15K slots)
                100000 + extraAnims[i].animId,
            ));
        }
        // number of extra animation (without empty slot)
        return idlePoseList.Size() - 1;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Management of animations for actor assets per storyboard shot.
//  - selecting animation from available (actor compatible) list of animations
//
class CModStoryBoardIdlePoseListsManager {
    // ------------------------------------------------------------------------
    private var compatiblePosesCount: int;
    protected var dataLoaded: Bool;
    protected var extraAnimCount: int;
    // ------------------------------------------------------------------------
    // contains info about all idle pose animations. the slot number for a pose
    // anim be used as id in the filtered UI listview. this is required as the UI
    // returns the selected option id as string and there is no string -> name
    // conversion available but playing animations requires the anim name as CName.
    // meaning: this array is also used as ui selected anim id -> cname anim id LUT
    protected var idlePoseMeta: CStoryBoardIdlePoseMetaInfo;
    // ------------------------------------------------------------------------
    public function init() { }
    // ------------------------------------------------------------------------
    protected function lazyLoad() {
        idlePoseMeta = new CStoryBoardIdlePoseMetaInfo in this;
        extraAnimCount = idlePoseMeta.addExtraAnimations(SBUI_getExtraIdleAnimations());
        idlePoseMeta.loadCsv("dlc\storyboardui\data\actor_idle_animations.csv");
        dataLoaded = true;
    }
    // ------------------------------------------------------------------------
    public function activate() {}
    // ------------------------------------------------------------------------
    public function deactivate() {}
    // ------------------------------------------------------------------------
    public function getIdlePoseListFor(actor: CModStoryBoardActor)
        : CModSbUiIdlePoseList
    {
        var actorIdlePoses: CModSbUiIdlePoseList;
        var i: int;

        if (!dataLoaded) { lazyLoad(); }

        actorIdlePoses = new CModSbUiIdlePoseList in this;
        compatiblePosesCount = actorIdlePoses.createCompatibleList(actor, idlePoseMeta);

        return actorIdlePoses;
    }
    // ------------------------------------------------------------------------
    public function getIdlePoseCount() : int {
        return compatiblePosesCount;
    }
    // ------------------------------------------------------------------------
    public function getDefaultIdleAnimFor(actor: CModStoryBoardActor) : CName {
        var result: CName;
        var type: int = (int)actor.getActorType();
        var i: int;

        if (!dataLoaded) { lazyLoad(); }

        if (type == (int)ESB_AT_Unknown) {
            // search all
            for (i = 0; i < idlePoseMeta.defaultPoseList.Size(); i += 1) {
                if (actor.isCompatibleAnimation(idlePoseMeta.defaultPoseList[i].animId))
                {
                    return idlePoseMeta.defaultPoseList[i].animId;
                }
            }

        } else {
            // two search runs (type specific + category as fallback):

            // 1. type specific
            for (i = 0; i < idlePoseMeta.defaultPoseList.Size(); i += 1) {
                if (idlePoseMeta.defaultPoseList[i].type == type &&
                    actor.isCompatibleAnimation(
                        idlePoseMeta.defaultPoseList[i].animId))
                {
                    return idlePoseMeta.defaultPoseList[i].animId;
                }
            }

            // 2. category specific (categories have 30 slots (as of now))
            type = (type / 30);
            for (i = 0; i < idlePoseMeta.defaultPoseList.Size(); i += 1) {

                if (idlePoseMeta.defaultPoseList[i].type / 30 == type &&
                    actor.isCompatibleAnimation(
                        idlePoseMeta.defaultPoseList[i].animId))
                {
                    return idlePoseMeta.defaultPoseList[i].animId;
                }
            }
        }

        return '';
    }
    // ------------------------------------------------------------------------
    public function getPoseInformation(selectedUiId: int) : SStoryBoardIdlePoseInfo {
        var info: SStoryBoardIdlePoseInfo;
        var i, s: int;

        if (!dataLoaded) { lazyLoad(); }

        if (selectedUiId >= 100000) {
            s = idlePoseMeta.idlePoseList.Size();
            for (i = 0; i < s; i += 1) {
                if (idlePoseMeta.idlePoseList[i].slotId == selectedUiId) {
                    info = idlePoseMeta.idlePoseList[i];
                    return info;
                }
            }
        } else if (selectedUiId >= 0) {
            info = idlePoseMeta.idlePoseList[extraAnimCount + selectedUiId];
        }
        return info;
    }
    // ------------------------------------------------------------------------
    public function getIdleAnimationName(selectedUiId: int) : CName {
        var i, s: int;

        if (!dataLoaded) { lazyLoad(); }

        if (selectedUiId >= 100000) {
            s = idlePoseMeta.idlePoseList.Size();
            for (i = 0; i < s; i += 1) {
                if (idlePoseMeta.idlePoseList[i].slotId == selectedUiId) {
                    return idlePoseMeta.idlePoseList[i].id;
                }
            }
        }

        return idlePoseMeta.idlePoseList[extraAnimCount + selectedUiId].id;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
